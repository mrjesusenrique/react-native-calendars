import React, { useState } from 'react';
import { StyleSheet, Text, View, TextInput, ScrollView, TouchableHighlight } from 'react-native';
import { Calendar, LocaleConfig } from 'react-native-calendars';

LocaleConfig.locales['fr'] = {
  monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
  monthNamesShort: ['Ene.', 'Feb.', 'Mar', 'Abr', 'May', 'Jun', 'Jul.', 'Ago', 'Sep.', 'Oct.', 'Nov.', 'Dic.'],
  dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
  dayNamesShort: ['Dom.', 'Lun.', 'Mar.', 'Mie.', 'Jue.', 'Vie.', 'Sab.'],
  today: 'Aujourd\'hui'
};
LocaleConfig.defaultLocale = 'fr';

export default function App() {

  const [inputName, setName] = useState('');
  const [inputLastName, setLastName] = useState('');
  const [inputAge, setAge] = useState('');
  const [inputAddress, setAddress] = useState('');

  const addName = (value) => {
    setName(value);
  };

  const addLastName = (value) => {
    setLastName(value);
  };

  const addAge = (value) => {
    setAge(value);
  };

  const addAddress = (value) => {
    setAddress(value);
  };

  return (
    <View style={{ padding: 30 }}>

      <ScrollView showsVerticalScrollIndicator={false}>

        <View>
          <Text style={{ textAlign: 'center', margin: 40, fontSize: 20, fontWeight: 'bold' }}>Por favor ingrese los datos requeridos</Text>

          <TextInput placeholder="Nombre" style={styles.input} onChangeText={addName} />
          <TextInput placeholder="Apellido" style={styles.input} onChangeText={addLastName} />
          <TextInput placeholder="Edad" style={styles.input} onChangeText={addAge} keyboardType='numeric' />
          <TextInput placeholder="Dirección" style={styles.input} onChangeText={addAddress} />
          <Text style={{ marginTop: 15, marginBottom: 15, marginLeft: 15, color: 'grey' }}>Disponibilidad</Text>

          <Calendar
            minDate={'2020-10-01'}
            maxDate={'2020-12-31'}
            onDayPress={(day) => { console.log('selected day', day) }}
            onDayLongPress={(day) => { console.log('selected day', day) }}
            monthFormat={'MMMM yyyy'}
            onMonthChange={(month) => { console.log('month changed', month) }}
            hideExtraDays={false}
            disableMonthChange={true}
            firstDay={1}
            onPressArrowLeft={subtractMonth => subtractMonth()}
            onPressArrowRight={addMonth => addMonth()}

            theme={{
              backgroundColor: '#ffffff',
              calendarBackground: '#ffffff',
              textSectionTitleColor: '#b6c1cd',
              textSectionTitleDisabledColor: '#d9e1e8',
              selectedDayBackgroundColor: '#00adf5',
              selectedDayTextColor: '#ffffff',
              todayTextColor: '#00adf5',
              dayTextColor: '#2d4150',
              textDisabledColor: '#d9e1e8',
              dotColor: '#00adf5',
              selectedDotColor: '#ffffff',
              arrowColor: '#002B36',
              disabledArrowColor: '#d9e1e8',
              monthTextColor: 'blue',
              indicatorColor: 'blue',
              textDayFontFamily: 'monospace',
              textMonthFontFamily: 'monospace',
              textDayHeaderFontFamily: 'monospace',
              textDayFontWeight: '300',
              textMonthFontWeight: 'bold',
              textDayHeaderFontWeight: '300',
              textDayFontSize: 16,
              textMonthFontSize: 16,
              textDayHeaderFontSize: 16
            }}
          />
        </View>

        <View style={{ marginTop: 50 }}>

          <TouchableHighlight style={styles.sendButton}>
            <Text style={styles.textButton}>Enviar Datos</Text>
          </TouchableHighlight>

        </View>

        <View>
          <Text style={{ textAlign: 'center', margin: 40, fontSize: 25, fontWeight: 'bold' }}>Sus datos son:</Text>
          <Text style={styles.textData}>{inputName}</Text>
          <Text style={styles.textData}>{inputLastName}</Text>
          <Text style={styles.textData}>{inputAge}</Text>
          <Text style={styles.textData}>{inputAddress}</Text>
        </View>

      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  input: {
    borderWidth: 1,
    borderColor: 'gray',
    padding: 3,
    margin: 15,
    borderRadius: 6,
    backgroundColor: '#E7E7E9'
  },

  sendButton: {
    width: 200,
    height: 50,
    backgroundColor: '#0099FF',
    borderRadius: 15,
    paddingHorizontal: 20,
    marginLeft: 80
  },

  textButton: {
    color: 'white',
    textAlign: 'center',
    fontSize: 25,
    marginTop: 10
  },

  textData: {
    textAlign: 'center',
    fontSize: 20,
  }
});
